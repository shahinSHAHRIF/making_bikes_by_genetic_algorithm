import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
##############################
from grader_func import grader
from animate_func import animate
from ChildGen import child_generator
from surface_func import surface
from friction_force_func import friction_force
from gravity_force_func import gravity_force
from time_evolution_func import time_evolution
from thrust_force_func import thrust_force
from spring_force_func import spring_force
from dv_vactor_calculator_func import dv_vactor_calculator
from d_surface_func import d_surface
from make_random_bike_func import make_random_bike
##############################

def main():
    bikes = make_random_bike()
    initial_velocities=np.zeros((1,8))
    for i in range(1000):
        bikes[i,8] = grader(bikes[i,0:8], initial_velocities)










'''
    fig, ax = plt.subplots()
    ax.set_xlim(0,50)
    ax.set_ylim(-1,5)
    #xw1 = dots[::1,0]              #This line will be updated soon when results come!
    #xw2 = dots[::2,0]              #This line will be updated soon when results come!
    #yw1 = dots[::1,1]              #This line will be updated soon when results come!
    #yw2 = dots[::2,1]              #This line will be updated soon when results come!
    #xm1 = dots[::1,0]              #This line will be updated soon when results come!
    #xm2 = dots[::2,0]              #This line will be updated soon when results come!
    #ym1 = dots[::1,1]              #This line will be updated soon when results come!
    #ym2 = dots[::2,1]              #This line will be updated soon when results come!
    x_surface=np.linspace(0,50,1000)
    y_surface=np.sin(x_surface)                                  #Defining the surface 

    wheel1, = ax.plot([],[],'b.',ms=30,fillstyle='full',marker='o')
    wheel2, = ax.plot([],[],'b.',ms=30,fillstyle='full',marker='o')
    mass1,=ax.plot([],[],'r.',ms=10,fillstyle='full',marker="^")
    mass2,=ax.plot([],[],'r.',ms=10,fillstyle='full',marker="^")
    sline, = ax.plot([],[],'y.',ms=5,fillstyle='full',marker="*")
    ani = animation.FuncAnimation(fig, animate, interval=25, blit=True)
    #ani.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
    # or
    plt.show()
'''
