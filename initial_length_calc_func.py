# -*- coding: utf-8 -*-
"""
Created on Tue May  8 11:26:57 2018

@author: SHAHIN
"""
import numpy as np

def initial_length_calc(positions):
    initial_length_mat=np.zeros((4,4))
    for i in range(0,4):
        for j in range(i+1,4):
            initial_length_mat[i,j] =np.sqrt( (positions[2*i]-positions[2*j])**2+ (positions[2*i+1]-positions[2*j+1])**2)
            initial_length_mat[j,i]= initial_length_mat[i,j]
    return initial_length_mat