import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
    
def animate(i):
'''
Animation function
input: x,y coordinates of 4 points (two wheels and two mass)
output: making a animation   
    hints: m: mass, w: wheel  - e.x: xm1=x-coordinate of first mass!
----------------------------------------------------------------------
 Based on 

 Animation of Elastic collisions with Gravity
 
 author: Jake Vanderplas
 email: vanderplas@astro.washington.edu
 website: http://jakevdp.github.com
 license: BSD
 Please feel free to use and modify this, but keep the above information. Thanks!
'''
    time_evolution()                  # Don't forget to put your physics_function here
    wheel1.set_data(xw1,yw1)  # update the data
    wheel2.set_data(xw2,yw2)  # update the data
    mass1.set_data(xm1,ym1)  # update the data
    mass2.set_data(xm2,ym2)  # update the data
    sline.set_data(x_surface,y_surface)
    return wheel1,wheel2,mass1,mass2,sline,
