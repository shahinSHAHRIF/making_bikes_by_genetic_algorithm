# -*- coding: utf-8 -*-
"""
Created on Mon May  7 11:21:41 2018

@author: SHAHIN
"""
import numpy as np
dv=np.zeros(1,8)
def dv_vector_calculator(positions,velocitys):
    """
    this part calculates the variation of force caused by spring
    """
    dv[1,0:1]=spring_force(positions[0:1],positions[2:3])+spring_force(positions[0:1],positions[4:5])+spring_force(positions[0:1],positions[6:7])
    dv[1,2:3]=-spring_force(positions[0:1],positions[2:3])+spring_force(positions[2:3],positions[4:5])+spring_force(positions[2:3],positions[6:7])
    dv[1,4:5]=spring_force(positions[4:5],positions[2:3])-spring_force(positions[0:1],positions[4:5])+spring_force(positions[4:5],positions[6:7])
    dv[1,6:7]=spring_force(positions[6,7],positions[2:3])+spring_force(positions[6:7],positions[4:5])-spring_force(positions[0:1],positions[6:7])
    """
    this part calculates the velocity dependent friction force
    """
    dv += -friction_coeficient*velocitys*dt/m
    """
    this part implements the thrust force made by wheel one
    """
    dv[1,0:1] += thrust_force(positions[0:1])
    """
    this part simulates gravity
    """
    dv[1,0:1]=gravity_force(positions[0:1])
    dv[1,2:3]=gravity_force(positions[2:3])
    dv[1,4:5]=gravity_force(positions[4:5])
    dv[1,6:7]=gravity_force(positions[6:7])

