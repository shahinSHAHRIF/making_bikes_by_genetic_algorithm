
# coding: utf-8

# In[29]:

import numpy as np
import random
from ChildGen import child_generator

def cg_test_1():
    SB = np.zeros((2,9))
    assert child_generator(SB,1) == np.zeros((1,9))
    
def cg_test_2():
    SB = np.zeros((2,9))
    SB [1,:] = 1
    assert child_generator(SB,1) == [0.5, 0.5 , 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0]

