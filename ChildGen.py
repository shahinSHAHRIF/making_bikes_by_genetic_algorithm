
# coding: utf-8

# In[3]:

#ISSUE #3

import numpy as np
import random
#SB = np.random.rand(10,9)
#SB is Selected Bikes, a matrix with bikes as rows and x1,y1,x2,y2,x3,y3,d as colums

def child_generator(SB, multiplication):
    """
    A function to generate children from parent bikes
    input: Selected bikes from the previous natural selection (a matrix with bikes as rows and x1,y1,x2,y2,x3,y3,d as colums)
    output: Children

    """
    NumberOfChildren = np.shape(SB)[0]*multiplication
    Child = np.zeros((NumberOfChildren,9))
    for i in range(0,NumberOfChildren):
        Parent1, Parent2 = random.sample(range(0, np.shape(SB)[0]), 2)
        for j in range(0,8):
            #Mutation rate is to generate a new race with a 
            MutationRate = random.uniform(-0.03, 0.03)
            Child[i,j] = (SB[Parent1,j] + SB[Parent2,j]) /2 + (MutationRate) 
    print(Child)
    return Child


# In[ ]:



