# -*- coding: utf-8 -*-
"""
this function takes two 2-d position  vector and calculates the 
force which acts on r1 particle.

@author: SHAHIN
"""
import numpy as np
def spring_force(r1,r2,l0):
    constant=k*dt/m*(1-l0/np.sqrt((r1[1]-r2[1])^2+(r1[2]-r2[2])^2))
    dv=np.zeros(1,2)
    dv[1]=constant*(r2[1]-r1[1])
    dv[2]=constant*(r2[2]-r1[2])
    return dv;